#!/usr/bin/env bash

function run {
  if ! pgrep -f $1 ;
  then
    $@&
  fi
}

mpd --kill
xrdb ~/.Xresources
picom -b --detect-rounded-corners --config ~/.config/picom.conf
setxkbmap -model abnt2 -layout br -variant abnt2
xset -b
